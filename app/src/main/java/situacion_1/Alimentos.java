package situacion_1;
public class Alimentos {
    
    private String descripcion;
    private int lipidos;
    private int hidratosDeCarbono; 
    private int proteinas;
    private String origenAnimal;
    private char contenidoDeVitaminas;
    public double valor; 


    public void setDescripcion(String descripcion){
        this.descripcion = descripcion;
    }

    public String getDescripcion(){
        return descripcion;
    }

    public void setLipidos(int lipidos){
        this.lipidos = lipidos;
    }

    public int getLipidos(){
        return lipidos;
    }

    public void setHidratosDeCarbono(int hidratosDeCarbono){
        this.hidratosDeCarbono = hidratosDeCarbono;
    }

    public int getHidratosDeCarbono(){
        return hidratosDeCarbono;
    }

    public void setProteinas(int proteinas){
        this.proteinas = proteinas;
    }

    public int getProteinas(){
        return proteinas;
    }

    public void setOrigenAnimal(String origenAnimal){
        this.origenAnimal = origenAnimal;
    }

    public String getOrigenAnimal(){
        return origenAnimal;
    }

    public void setContenidoDeVitaminas(char contenidoDeVitaminas){
        this.contenidoDeVitaminas = contenidoDeVitaminas;
    }

    public char getContenidoDeVitaminas(){
        return contenidoDeVitaminas;
    }

    public String compararLipidos(double lipidos, char contenidoDeVitaminas){
        if(lipidos < 20.00){
            if(contenidoDeVitaminas == 'A' ){
                return "Es dietetico";
            }
            else if(contenidoDeVitaminas == 'M'){
                return "Es dietetico";
            }
            else{
                return "No es dietetico";
            }
        }
        else {
            return "No es dietetico";
        }
    }

    public String deportistas(int proteinas, int hidratosDeCarbono, int lipidos){
        if(proteinas>= 10.00 && proteinas<=15){
            if(hidratosDeCarbono>=55.00 && hidratosDeCarbono<=65.00){
                if(lipidos>=30.00 && lipidos<=35.00){
                    return "Recomendado para deportistas";
                }
                else{
                    return "No recomendado para deportistas";
                }
            }
            else{
                return "No recomendado para deportistas";
            }
        } else{
            return "No recomendado para deportistas";
        }
    }

    public double ValorEnergrtico(int lipidos, int hidratosDeCarbono, int proteinas){
        valor = (lipidos*9.4)+ (hidratosDeCarbono*4.1)+(proteinas*5.3);
        return valor;
    }

    public String toString(){
        return "Descripcion: " + descripcion + ". Contenido de Lipidos: " + lipidos +
        ". Contenido de hidratos de carbono: " + hidratosDeCarbono + 
        ". Contenido de proteinas: " + proteinas + ". Es de origen animal: " + origenAnimal +
        ". Contenido de Vitaminas: " + contenidoDeVitaminas + ".   "+ compararLipidos(lipidos, contenidoDeVitaminas) +
        ".   " + deportistas(proteinas, hidratosDeCarbono, lipidos) +
        ".  Valor energetico: " +  ValorEnergrtico(lipidos, hidratosDeCarbono, proteinas);
    }
    
}
