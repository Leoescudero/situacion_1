package situacion_1;

public class Principal {
    
    public static void main(String[] args) {
        
        Alimentos alimento1 = new Alimentos();
        Alimentos alimento2 = new Alimentos();


        alimento1.setDescripcion("Arroz");
        alimento1.setLipidos(19);
        alimento1.setHidratosDeCarbono(35);
        alimento1.setProteinas(25);
        alimento1.setOrigenAnimal("No");
        alimento1.setContenidoDeVitaminas('A');

        alimento1.compararLipidos(16, 'A');
        alimento1.deportistas(25, 35, 16);
        alimento1.ValorEnergrtico(16, 35, 25);

        alimento2.setDescripcion("Yogurt");
        alimento2.setLipidos(32);
        alimento2.setHidratosDeCarbono(58);
        alimento2.setProteinas(12);
        alimento2.setOrigenAnimal("Si");
        alimento2.setContenidoDeVitaminas('B');

        alimento2.compararLipidos(32, 'B');
        alimento2.deportistas(12, 58, 32);
        alimento2.ValorEnergrtico(36, 58, 12);

        System.out.println( " " + alimento1.toString());
        System.out.println( "  ");
        System.out.println( "  ");
        System.out.println( " " + alimento2.toString());

    }
}
