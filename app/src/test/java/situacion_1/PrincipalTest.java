package situacion_1;
import static org.junit.Assert.assertEquals;

import org.junit.Test; 

public class PrincipalTest {
    @Test public void setDescripcionTest(){
        Alimentos descripcion = new Alimentos();
        descripcion.setDescripcion("Yogurt");

        String descripcionactual = descripcion.getDescripcion();

        assertEquals("Yogurt", descripcionactual);
    }
}
